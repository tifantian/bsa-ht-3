const Commentator = require('./comment-bot/Commentator');

const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
// const morgan = require('morgan');
const passport = require('passport');
const cors = require('cors');
const indexRouter = require('./routes/index.js');
const loginRouter = require('./routes/login.js');
const gameFieldRouter = require('./routes/gamefield.js')
const giveTextRouter = require('./routes/texts.js');
const startGame = require('./routes/startgame.js')

require('./passport.config');

server.listen(7777);
const texts = require('./textsJSON/texts');
console.log('Listen PORT localhost:7777');

// app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/gamefield', gameFieldRouter);
app.use('/texts', giveTextRouter);
app.use('/startgame', startGame);


const objectUsers = {};

const defaultValueOfSeconds = 20;  /// Тут можно поменять таймер
const lengthTextsArray = Object.keys(texts).length;
let randomValue = 1 + Math.floor(Math.random() * (lengthTextsArray));
let isGameStarted = false;
let secondsToStart = defaultValueOfSeconds;
let timerID;
const allSockets = [];
let globalTextLength = 1000;
const objLoginSocket = {};

const commentator = new Commentator('Bobik'); // singleton

io.on('connection', socket => {
  allSockets.push(socket);
  socket.on('newPlayer', payload => {
    const { token } = payload;
    const userLogin = jwt.decode(token).login;
    const index = allSockets.indexOf(socket);
    allSockets[index].userName = userLogin;
    objLoginSocket[userLogin] = socket;
    if (Object.keys(objectUsers).length == 0) {
      makeNewTimer(secondsToStart);
    }
    objectUsers[userLogin] = 0;
    console.log(`${userLogin} connect`);
    if (isGameStarted) {
      socket.emit('showMessageWait');
      return;
    }
    socket.broadcast.emit('newPlayer', { objectUsers, secondsToStart });
    socket.emit('newPlayer', { objectUsers, secondsToStart });

    const oneUserObject = { userLogin, score: 0 };
    const newMessage = commentator.addPlayer(oneUserObject);

    socket.broadcast.emit('newMessage', { newMessage });
    socket.emit('newMessage', { newMessage });


  });

  socket.on('currentPosition', payload => {
    const { totalSeconds } = payload;
    if (totalSeconds % 30 === 0 && totalSeconds !== 120 && totalSeconds !== 0) {
      const newMessage = commentator.currentPosition(objectUsers, totalSeconds);
      socket.broadcast.emit('newMessage', { newMessage });
      socket.emit('newMessage', { newMessage });
    } else if(totalSeconds % 15 === 0 && totalSeconds !== 0) {
      const newMessage = commentator.sayJoke(objectUsers);
      socket.broadcast.emit('newMessage', { newMessage });
      socket.emit('newMessage', { newMessage });
    }
    
  });

  socket.emit('getTextId', { randomValue });

  socket.on('gameFinished', payload => {
    isGameStarted = false;
    const { timeToPlay, totalSeconds } = payload;
    const newMessage = commentator.finishedGame(objectUsers);
    socket.broadcast.emit('newMessage', { newMessage });
    socket.emit('newMessage', { newMessage });

    socket.broadcast.emit('gameFinished');
    socket.emit('gameFinished');
  });

  socket.on('countScores', payload => { // количество введенных символов
    const { score, token, totalSeconds } = payload;
    const userLogin = jwt.decode(token).login;
    objectUsers[userLogin] = score;

    if(globalTextLength - score === 30) {
      const newMessage = commentator.finishSoon(objectUsers);
      socket.broadcast.emit('newMessage', { newMessage });
      socket.emit('newMessage', { newMessage });
    }

    socket.broadcast.emit('countScores', { score, user: userLogin });
    socket.emit('countScores', { score, user: userLogin });
  });



  socket.on("disconnect", () => {
    clearInterval(timerID);
    const index = allSockets.indexOf(socket);
    const disconnectedUser = allSockets[index].userName;

    delete objectUsers[disconnectedUser];
    console.log(`${disconnectedUser} disconnect`);
    if (Object.keys(objectUsers).length === 0) {
      isGameStarted = false;
      secondsToStart = defaultValueOfSeconds;
      randomValue = 1 + Math.floor(Math.random() * (lengthTextsArray));
    }

    const oneUserObject = { disconnectedUser, score: 0 };
    const newMessage = commentator.removePlayer(oneUserObject);

    socket.broadcast.emit('newMessage', { newMessage });
    socket.emit('newMessage', { newMessage });

    socket.broadcast.emit('deletePlayer', disconnectedUser);
    socket.emit('deletePlayer', disconnectedUser);

    // Когда все дисконектнулись сгенерировать новую 
  });

  function startGame() {
    isGameStarted = true;
    socket.broadcast.emit('startGame');
    socket.emit('startGame');
    socket.on('textLength', (payload) => {
      const { textLength } = payload;
      globalTextLength = textLength;
      setTimeout(() => {
        const newMessage = commentator.afterStart(textLength);
        socket.broadcast.emit('newMessage', { newMessage });
        socket.emit('newMessage', { newMessage });
      }, 5000)
    });
  }


  function makeNewTimer(timeInSeconds) {
    secondsToStart = timeInSeconds;
    timerID = setInterval(() => {
      secondsToStart--;
      if (secondsToStart <= 5) {
        const newMessage = commentator.showTimeToStart(secondsToStart);

        socket.broadcast.emit('newMessage', { newMessage });
        socket.emit('newMessage', { newMessage });
      }
      if (secondsToStart === 0) {
        secondsToStart = defaultValueOfSeconds;
        startGame();
        clearInterval(timerID);
      }
    }, 1000);
  }
});

