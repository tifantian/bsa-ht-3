var express = require("express");
var router = express.Router();
const path = require('path');
const passport = require('passport'); 
const jwt = require('jsonwebtoken');
require('../passport.config.js');

// router.use('/', passport.authenticate('jwt', {session: false}));


router.get('/',  passport.authenticate('jwt', {session: false}),  function(req, res) {
    res.sendFile(path.join(__dirname, '..', 'textsJSON', 'texts.json'));
});

module.exports = router;