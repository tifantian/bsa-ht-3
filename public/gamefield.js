window.onload = () => {
    
    const jwt = localStorage.getItem('jwt');
    if(!jwt) {
        location.replace('/login');
    } else {

        const buttonStartGame = document.getElementById('ready');

        buttonStartGame.addEventListener('click', () => {
            location.replace('/startgame');
        });
       
    }
}