const fs = require('fs');

const handler = {
    get: (target, property) => {
        if(property in target) {
            return target[property]
        } else {
            return 'Без машины'
        } 
    }
}

const cars = fs.readFileSync('./comment-bot/cars.json');
const jsonCars = JSON.parse(cars);
const p = new Proxy(jsonCars, handler); // юзаю прокси

function getTransport() {
    const number = Math.floor(Math.random() * 8);
    return p[number];
}

module.exports = getTransport;