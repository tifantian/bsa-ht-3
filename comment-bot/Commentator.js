const getTransport = require('./car');

class Commentator {
    constructor(name) {
        this.commentator = name;
        this.allUsers = [];
    }    

    addPlayer(userObject) {
        userObject.transport = getTransport(); // proxy
        
        this.allUsers.push(userObject);
        this.user = `[${userObject.transport}] ${userObject.userLogin}`;
        return this.createMessage('addPlayer');
    }

    finishSoon(objectUsers) {
        for(let i = 0; i < this.allUsers.length; i++) {
            this.allUsers[i].score = objectUsers[this.allUsers[i].userLogin];
        }
        return this.createMessage('finishSoon');
    }

    finishedGame(objectUsers) {
        for(let i = 0; i < this.allUsers.length; i++) {
            this.allUsers[i].score = objectUsers[this.allUsers[i].userLogin];
        }
        return this.createMessage('finishedGame');
    }
    
    sayJoke(objectUsers) {
        for(let i = 0; i < this.allUsers.length; i++) {
            this.allUsers[i].score = objectUsers[this.allUsers[i].userLogin];
        }
        return this.createMessage('sayJoke');
    }

    showTimeToStart(time) {
        this.time = time;
        return this.createMessage('showTimeToStart');
    }

    removePlayer(userObject) {
        let index = -1;
        this.disconnectedUser = userObject.disconnectedUser;
        
        for(let i = 0; i < this.allUsers.length; i++) {
            if(this.allUsers[i].userLogin == userObject.disconnectedUser) {
                index = i;
            }
        }
        this.allUsers.splice(index, 1);
        return this.createMessage('removePlayer');
    }

    afterStart(textLength) {
        this.textLength = textLength;
        return this.createMessage('afterStart');
    }

    currentPosition(objectUsers, secondsToFinish) {
        this.secondsToFinish = secondsToFinish;
        for(let i = 0; i < this.allUsers.length; i++) {
            this.allUsers[i].score = objectUsers[this.allUsers[i].userLogin];
        }
        return this.createMessage('currentPosition');
    }

    

    createMessage(type) { // Factory
        let message = `${this.commentator}: `;
        let newComment;
        if(type === 'addPlayer') {
            newComment = new Player(this.allUsers, this.user, this.commentator);
        }
        if(type === 'showTimeToStart') {
            newComment = new Time(this.time);
        }
        if(type === 'removePlayer') {
            newComment = new RemovePlayer(this.disconnectedUser);
        }
        if(type === 'afterStart') {
            newComment = new AfterStart(this.textLength);
        }
        if(type === 'currentPosition') {
            newComment = new CuurentPosition(this.allUsers, this.secondsToFinish, this.textLength);
        }
        if(type === "sayJoke") {
            newComment = new Joke(this.allUsers); 
        }
        if(type === "finishSoon") {
            newComment = new FinishSoon(this.allUsers);
        }
        if(type === 'finishedGame') {
            newComment = new FinishedGame(this.allUsers, this.commentator);
        }
        message += newComment.getMessage();
        return message;
    }
}

class Player {
    constructor(allUsers, user, commentator) {
        this.allUsers = allUsers;
        this.user = user;
        this.commentator = commentator;
    }

    getMessage() {
        let message = "";
        if(this.allUsers.length === 1) {
            message += `Всем привет! Меня зовут ${this.commentator}. К нам подключился первый игрок ${this.user}! Пожелаем ему удачи в этой нелегкой битве`;
        } else {
            message += `Новый игрок! Поприветствуем ${this.user}! `;
            let usersInString = "Наши игроки: ";
            for(let i = 0; i < this.allUsers.length; i++) {
                usersInString += this.allUsers[i].userLogin + ' ';
            }
            message += usersInString + ' с нетерпением ждут начала битвы'; 
        }
        return message;
    }
}

class Time {
    constructor(seconds) {
        this.seconds = seconds;
    }

    getMessage() {
        let nameForSeconds;
        let message = "";
        if(this.seconds == 5) {
            nameForSeconds = 'секунд'
        } else if(this.seconds >= 2) {
            nameForSeconds = 'секунды'
        } else {
            nameForSeconds = 'секунда';
        }
        if(this.seconds === 0) {
            message += `Поехали !!! Быстрей, быстрееей!!!`;
        } else {
            message += `Внимание! скоро начнется игра !! Осталось ${this.seconds} ${nameForSeconds}`;
        }
        return message;
    }
}

class RemovePlayer {
    constructor(user) {
        this.user = user;
    }

    getMessage() {
        return `${this.user} испугался и вышел! Теперь он подсвечивается красным, неудачник!`;
    }
}

class AfterStart {
    constructor(textLength) {
        this.textLength = textLength;
    }

    getMessage() {
        return `Игрокам предстоит ввести много сивмолов! Аж ${this.textLength}! Советую поторопиться!`;
    }
}

class CuurentPosition {
    constructor(allUsers, secondsToFinish, textLength) {
        this.allUsers = allUsers;
        this.secondsToFinish = secondsToFinish;
        this.textLength = textLength;
    }

    getMessage() {
        let message = "Сейчас гонка выглядит так: ";
        this.allUsers.sort((b, a) => a.score - b.score);
        if(this.allUsers.length >= 1) {
            const percent = this.getPercent(this.allUsers[0].score, this.textLength);
            message += `Первым идет [${this.allUsers[0].transport}] ${this.allUsers[0].userLogin}, у него ${percent}%.. `
        }
        if(this.allUsers.length >= 2) {
            const percent = this.getPercent(this.allUsers[1].score, this.textLength);
            message += `Вторым же идет [${this.allUsers[1].transport}] ${this.allUsers[1].userLogin}, у него ${percent}%.. `;
        }
        if(this.allUsers.length >=3 ) {
            message += `А вот остальные игроки отстают, но ${this.allUsers[2].userLogin} еще имеет не плохие шансы.. `;
        }
        return message + `Осталось всего ${this.secondsToFinish} секунд. Вперёд!`;
    }

    getPercent(min, max) {
        return Math.floor((min / max) * 100); 
    }
}

class Joke { 
    constructor(allUsers) {
        this.allUsers = allUsers;
        this.folder = new Folder(); 
        this.jokeList = new JokeList(this.allUsers); // Facade
    }

    getMessage() {
        this.folder.open();
        const message = this.jokeList.find();
        return message;
    }


}

class Folder {
    open() {
        const message = '...Folder Opened...';
        return message;
    }
    close() {
        const message = '...Folder Closed...';
        return message;
    }
}

class JokeList { // Facade
    constructor(allUsers) {
        this.allUsers = allUsers;
    }
    find() {
        const randomIndex = Math.floor(Math.random() * this.allUsers.length);
        const userName = `[${this.allUsers[randomIndex].transport}] ${this.allUsers[randomIndex].userLogin}`;
        const jokes = [
            `Интересно... Кто же сегодня победит. Моя бабушка ставит на ${userName}`,
            `Вы только посмотрите что вытворяет ${userName}`,
            `${userName}, из какого ты города? Я приеду чтобы взять автограф.`,
            `Товарищи!!! Продолжайте топить! На кону не просто победа!! А нет, просто победа..`,
            `Я слышу как у ${userName} стучит сердце.. Как же он переживает, продолжай набирать текст!!`,
            `Даже не знаю кто выиграет, мне кажется что у ${userName} есть неплохие шансы`,
            `Как сказал мой дед: я твой дед.. ${userName}, а как твой дед говорил?`
        ];
        const randomJoke = Math.floor(Math.random() * jokes.length);
        return jokes[randomJoke];
    }
}

class FinishSoon {
    constructor(allUsers) {
        this.allUsers = allUsers;
    }

    getMessage() {
        this.allUsers.sort((b, a) => a.score - b.score);
        return `[${this.allUsers[0].transport}] ${this.allUsers[0].userLogin} уже очень близок к финишу!!! Ему осталось 30 символов`;
    }
}

class FinishedGame {
    constructor(allUsers, commentator) {
        this.allUsers = allUsers;
        this.commentator = commentator;
    }

    getMessage() {
        this.allUsers.sort((b, a) => a.score - b.score);
        let message = "Внимание! Конец гонки!!! ";
        if(this.allUsers.length >= 1) {
            message += `Первым финиширует [${this.allUsers[0].transport}] ${this.allUsers[0].userLogin}. `;
        }
        if(this.allUsers.length >= 2) {
            message += `Второе место занимает [${this.allUsers[1].transport}] ${this.allUsers[1].userLogin}. `
        }
        if(this.allUsers.length >= 3) {
            message += `Третье место осталось за [${this.allUsers[2].transport}] ${this.allUsers[2].userLogin}. `
        }
        return message + ` А с Вами был комментатор ${this.commentator}.. До скорых встреч!!! Чтобы начать новую игру закройте все вкладки и перезапустите сервер.`;
    }
}

module.exports = Commentator;